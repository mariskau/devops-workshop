import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.calculate(expression),1.0f);

        expression = " 300 - 99 ";
        assertEquals(201, calculatorResource.calculate(expression),1.0f);
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.calculate(expression),1.0f);

        expression = "300+99";
        assertEquals(399, calculatorResource.calculate(expression),1.0f);
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.calculate(expression),1.0f);

        expression = "20-2";
        assertEquals(18, calculatorResource.calculate(expression),1.0f);
    }
    @Test
    public void testMultiplication(){

        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "40*5";
        assertEquals(200, calculatorResource.calculate(expression),1.0f);
        
        expression = "32*9";
        assertEquals(288, calculatorResource.calculate(expression),1.0f);

    }
      @Test
    public void testDivision(){

        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "30/5";
        assertEquals(6.0f, calculatorResource.calculate(expression),1.0f);
        
        expression = "10/5";
        assertEquals(2.0f, calculatorResource.calculate(expression),1.0f);

    }
}
