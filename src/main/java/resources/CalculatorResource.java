package resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Calculator resource exposed at '/calculator' path
 */
@Path("/calculator")
public class CalculatorResource {

    /**
     * Method handling HTTP POST requests. The calculated answer to the expression will be sent to the client as
     * plain/text.
     * @param expression the expression to be solved as plain/text.
     * @return solution to the expression as plain/text or -1 on error
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)

/**
 * Calculator resource exposed at '/calculator' path
 */


    /**
     * Method handling HTTP POST requests. The calculated answer to the expression will be sent to the client as
     * plain/text.
     *
     * @param expression the expression to be solved as plain/text.
     * @return solution to the expression as plain/text or -1 on error
     */


    public float calculate(String expression) {
        // Removes all whitespaces

        String expressionTrimmed = expression.replaceAll("\\s+", "");

        float result = -1;

        /*
         * .matches use regex expression to decide if a String matches a given pattern.
         * [0-9]+[+][0-9]+ explained:
         * [0-9]+: a number from 0-9 one or more times. For example 1, 12 and 123.
         * [+]: the operator + one time.
         * The total regex expression accepts for example:
         * 12+34,
         * 1+2,
         * 10000+1000
         */
        String[] expre = expressionTrimmed.split("[+-/*]");
        String[] oper = expressionTrimmed.split("[0-9]+");
        boolean done = false;
        float tot = Float.parseFloat(expre[0]);

        int i = 1;

        for (i = 1; i < expre.length; i++) {
            if (oper.length == 2)
                done = true;


            if (oper[i].equals("+") && i + 1 < oper.length) {

                if (!oper[i + 1].equals("*") && !oper[i + 1].equals("/")) {

                    tot += Float.parseFloat(expre[i]);

                } else if (oper[i + 1].equals("*") || oper[i + 1].equals("/")) {


                    int iter = i + 1;
                    float subtot = Float.parseFloat(expre[i]);


                    while (oper[iter].equals("*") || oper[iter].equals("/")) {

                        if (oper[iter].equals("*")) {
                            subtot *= Float.parseFloat(expre[iter]);

                        } else if (oper[iter].equals("/")) {
                            subtot /= (Float.parseFloat(expre[iter]));

                        }


                        iter++;
                        i++;

                        if (i + 1 >= oper.length) {

                            tot += subtot;
                            done = true;
                            break;
                        }

                    }
                    if (done)
                        break;

                    tot += subtot;

                }
            }
            if (i + 1 >= oper.length && (oper[i].equals("+"))) {
                tot += Float.parseFloat(expre[i]);
                break;

            }


            if (oper[i].equals("-") && i + 1 < oper.length) {

                if (!oper[i + 1].equals("*") && !oper[i + 1].equals("/")) {

                    tot -= Float.parseFloat(expre[i]);
                } else if (oper[i + 1].equals("*") || oper[i + 1].equals("/")) {
                    int iter = i + 1;

                    float subtot = Float.parseFloat(expre[i]);

                    while (oper[iter].equals("*") || oper[iter].equals("/")) {

                        if (oper[iter].equals("*")) {

                            subtot *= Float.parseFloat(expre[iter]);

                            if (i + 1 == oper.length) {
                                done = true;
                                continue;
                            }
                        } else if (oper[iter].equals("/")) {
                            subtot /= (Float.parseFloat(expre[iter]));
                            if (i + 1 == oper.length) {
                                done = true;
                                continue;
                            }
                        }


                        iter++;
                        i++;

                        if (i + 1 >= (oper.length)) {

                            tot -= subtot;
                            done = true;
                            break;
                        }


                    }
                    if (done)
                        break;

                    tot -= subtot;

                    continue;

                }
            }
            if (i + 1 >= oper.length && oper[i].equals("-")) {
                tot -= Float.parseFloat(expre[i]);
                break;

            }
//**************************************  IF MULTIPLIERS OG DIVISORS  *****************************************************

            if ((oper[i].equals("*") || oper[i].equals("/")) && i + 1 < oper.length) {
                int iter = i + 1;


                if (oper[i].equals("*")) {
                    tot *= Float.parseFloat(expre[i]);

                } else if (oper[i].equals("/")) {
                    tot /= Float.parseFloat(expre[i]);

                }


                while ((oper[iter].equals("*") || oper[iter].equals("/"))) {


                    if (oper[iter].equals("*")) {
                        tot *= Float.parseFloat(expre[iter]);

                    } else if (oper[iter].equals("/")) {
                        tot /= (Float.parseFloat(expre[iter]));


                    }


                    iter++;
                    i++;
                    if (i + 1 >= oper.length) {
                        break;
                    }


                }


            }
            if (i + 1 >= oper.length && done == true) {


                if (oper[i].equals("*"))
                    tot *= Float.parseFloat(expre[i]);


                if (oper[i].equals("/"))
                    tot /= Float.parseFloat(expre[i]);
                break;
            }


        }


        
        return tot;
    }

}


